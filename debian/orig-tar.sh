#!/bin/sh -e

# called by uscan with '--upstream-version' <version> <file>
TAR=../jakarta-ecs_$2.tar.gz
DIR=jakarta-ecs-$2.orig

# clean up the upstream tarball
mv $TAR $3
tar zxf $3
mv jakarta-ecs-$2 $DIR
GZIP=--best tar czf $3 --exclude '*jar' --exclude 'site' --exclude '*dll' $DIR
rm -rf $TAR $DIR

# move to directory 'tarballs'
if [ -r .svn/deb-layout ]; then
    . .svn/deb-layout
    mv $3 $origDir
    echo "moved $3 to $origDir"
fi

exit 0
